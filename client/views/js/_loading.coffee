Template._loading.rendered = ->

  $('body').addClass('is-loading');

  # If the route is 'reset-all' run the reset function and then go back
  # to the main draft room route.
  if _.contains( ['/reset-all'], Router.current().route.path('/reset-all') )

    league = Router.current().params.league || 'duff'
    Meteor.call 'reset_all', league, () ->
      Router.go '/'

Template._loading.destroyed = ->

  $('body').removeClass('is-loading');