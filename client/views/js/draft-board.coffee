Template.DraftBoard.helpers 
  
  templateGestures: 
  
    'tap #slider_menu .toggler.button': (event, templateInstance) ->
      event.srcEvent.stopImmediatePropagation();
      $('#slider_menu').toggleClass('active')
      $('#slider_menu .toggler i').toggleClass 'fa-chevron-up fa-chevron-down'
      $('body').toggleClass('draft-board-active');

Template.DraftBoard.rendered = ->
  
  $('#slider_menu').addClass('active')
  $('body').addClass('draft-board-active');

  # Trigger menu functions
  $('a#menu, #navigation a').on 'click', (e) -> 
    $('body, #navigation, a#menu').toggleClass '__push left'
    $('a#menu i').toggleClass 'fa-bars fa-times'
    return