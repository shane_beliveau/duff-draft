Template.teams.onRendered ->

  # Constants
  $links = $('.teams ul li a')

  # If there are no active elements, just make the first one active
  if !$links.hasClass('active')
    $links.eq(0).addClass('active')

  # Handle the fake list clicks
  $links.on 'click', (e) ->
    
    $links.removeClass('active')
    $this = $(this);
    $this.addClass 'active'