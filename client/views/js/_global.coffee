Template._global.rendered = ->

  # Trigger menu functions
  $('a#menu, #navigation a').on 'click', (e) -> 
    $('body, #navigation, a#menu').toggleClass '__push left'
    $('a#menu i').toggleClass 'fa-bars fa-times'
    return

  # Global fix for fixed position when iOS keyboard open
  $('input[type="text"], textarea').on "blur", ->
    $timeout (()->
      $(".search-container").removeClass "active"
      $('div.bottom-bar').css('position', 'static')
      $('div.header').css('position', 'static')
      
      $timeout (()->
        $('div.bottom-bar').css('position', 'fixed')
        $('div.header').css('position', 'fixed')
      ), 200
      
    ), 200