Template.DraftRoom.onRendered ->

  window.getOnTheClockTimer = (force_reset) ->

    # Clear the interval if force
    if force_reset
      clearInterval window.timerInterval

    # Variables
    _draft = _Draftboard.findOne()
    time_limit = _draft.clock
    expire_at = _draft.expire_at
    _now = new Date().getTime()

    if force_reset or !expire_at
      time_left = time_limit
      expire_at = time_limit * 1000 + _now
      _Draftboard.update _draft._id, 
        $set: 
          expire_at: expire_at

    window.timerInterval = setInterval(->

      time_left = 0
      _now = new Date().getTime()

      if expire_at and expire_at > _now
        time_left = Math.ceil (expire_at - _now) / 1000
        time_left = if time_left > 0 then time_left else 0

      minutes = parseInt(time_left / 60, 10);
      seconds = parseInt(time_left % 60, 10);
      seconds = if seconds < 10 then '0' + seconds else seconds

      if time_left
        $('.on-the-clock .timer').html minutes + ':' + seconds
      else
        $('.on-the-clock .timer').html 'Time\'s Up!'
        clearInterval window.timerInterval
    , 1000)

  window.getOnTheClockTimer()

  # Allow tablesorting
  $('table').tablesort()
  window.tablesort = $('table').data('tablesort')
  $('thead th.int').data 'sortBy', (th, td, tablesort) ->
    parseInt td.text()

  player_item  = '.page--players tr:not(.featured)[data-position]'

  # Toggle player actions
  $(document).off 'click', player_item
  $(document).on 'click', player_item, (e) ->

    $(this)
      .toggleClass('open')
      .siblings()
      .removeClass('open')

  # Switch between list of players per position
  $(document).off 'click', '.page--players .ui--sub-menu li:not(.search) a'
  $(document).on 'click', '.page--players .ui--sub-menu li:not(.search) a', (e) ->
    
    e.preventDefault()

    $this         = $(e.target)
    reveal        = $this.data('path')
    player_item   = $('.page--players tr[data-position]')

    $this.parent().siblings().find('a').removeClass('active')
    $this.addClass('active')

    if reveal is 'all'
      player_item
        .css('display','')
    else 
      player_item
        .css('display','none')
        .siblings('[data-position="'+reveal+'"]')
        .css('display','')
    
    # Make the first player featured
    # window.feature_player()

  # Search bar controls
  $(document).off 'click', '.page--players .ui--sub-menu li.search'
  $(document).on 'click', '.page--players .ui--sub-menu li.search', (e) ->
    
    e.preventDefault()

    if $(e.target).attr('id') is 'player_search'
      return

    $this     = $(this)
    $search   = $this.find('#player_search')
    $icon     = $this.find('i')
    reveal    = $('.page--players .ui--sub-menu li a.active').data('path') or 'all'
    $players  = $('.page--players tr[data-position]')

    $search.toggleClass('active')

    if $icon.hasClass('fa-times')
      $icon.removeClass('fa-times').addClass('fa-search')
      $search.val('').trigger('keydown')

      if reveal is 'all'
        $players
          .css('display','')
      else 
        $players
          .css('display','none')
          .siblings('[data-position="'+reveal+'"]')
          .css('display','')

    else
      $icon.removeClass('fa-search').addClass('fa-times')
      $players.css('display','')

  # Search bar filter
  $('#player_search').fastLiveFilter('.page--players .ui--list tbody');
    

Template.DraftRoom.events
  
  'click a[data-action="draft-player"]' : ->

    $this = this
    
    $('.ui.basic.modal').modal(
      
      closable: false
      detachable: true
      autofocus: true
      keyboardShortcuts: true

      # Update the player's name in the modal option.
      onShow: ->
        $(this)
          .find('[data-player]')
          .html( $this.first_name + ' ' + $this.last_name)

      # Hacky way to fix bug that opens all player actions when
      # a modal window is closed.
      onHidden: ->
        $('.page--players tr:not(.featured)[data-position] .player--actions')
          .toggle()

        setTimeout ()->
          $('.page--players tr:not(.featured)[data-position] .player--actions')
          .toggle()
        , 250

      onDeny: ->
        return

      onApprove: ->
        
        # Update the player collection to show the selected player as drafted
        selected_player = $this
        pick = _Draftboard.findOne {}

        # Get the selected player document
        selected_player = _Players.findOne(_id: selected_player._id)
        
        # Double check to see if the player was already drafted or not      
        if selected_player.drafted
          alert "Player was already drafted. Select another player."
          return
        else
          selected_player.drafted = pick

        # Add the player to the appropriate team and determine the next
        # team in the draft order. Also insert overall draft information.
        Meteor.call 'find_by_order', {}, (error, team) ->

          # Begin transaction
          tx.last_transaction = tx.start('draft player')

          # Find the next pick
          _draft     = _Draftboard.findOne {}
          _overall   = _draft.overall + 1
          _round     = Math.floor((_overall - 1) / _draft.total_teams + 1)
          _pick      = (_overall - 1) % _draft.total_teams + 1
          
          Meteor.call 'get_next_pick', _overall, (error, next) ->
          
            _Draftboard.update _draft._id,
              $set:
                round     : _round
                pick      : _pick
                team_pick : next.order
                overall   : _overall 
                expire_at : null
            ,
              tx: true

            # Set the selected player status as drafted
            _Players.update selected_player._id,
              $set: 
                drafted: 1
            ,
              tx: true
            
            # Add the player to the team
            _Teams.update team._id, 
              $addToSet: 
                players: selected_player
            ,
              tx: true
            
            # Update the running draft board
            _Picks.insert
              overall: pick.overall
              round: pick.round
              pick: pick.pick
              team_id: team._id
              player_id: selected_player._id
            ,
              tx: true

            # End transaction
            tx.commit()

            return
          
          return

        return

    ).modal 'show'
