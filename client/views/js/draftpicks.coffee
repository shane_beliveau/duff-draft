Template.DraftPicks.events

  'change select': (e,t) ->

    _option = $(e.target).find("option:selected")
    _team = _Teams.findOne( _id: _option.attr('data-team-id') )
    _order = _option.attr('data-order')
    _overall = _option.attr('data-overall')

    _matrix = _PickMatrix.findOne(overall: +_overall)

    _PickMatrix.update _matrix._id,
      $set:
        team: _team