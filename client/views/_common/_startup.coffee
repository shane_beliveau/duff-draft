Meteor.startup ->

  # In-browser alert init
  sAlert.config
    effect: 'stackslide',
    position: 'bottom-right',
    timeout: 5000,
    html: false,
    onRouteClose: true,
    stack: true,
    # beep: '/sounds/espn_alert.mp3'
    offset: 25