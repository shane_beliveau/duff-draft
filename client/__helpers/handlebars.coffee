UI.registerHelper 'log', (str) ->  
  console.log(str)

# Convert to lowercase
# {{ to_lower_case @string }}
Handlebars.registerHelper 'to_lower_case', (str) ->
    str.toLowerCase()

# Convert to uppercase
# {{ to_upper_case @string }}
Handlebars.registerHelper 'to_upper_case', (str) ->
    str.toUpperCase()

# Convert to slug
# {{ slugify @string }}
Handlebars.registerHelper 'slugify', (string) ->
    return s.slugify(string)

# @@ Determines if current route is the player list
# {{ subActive }}
Handlebars.registerHelper 'subActive', (path) ->
  if !Router.current()
    return
  if path is Router.current().route.path()
    return 'active'
  else if ( ( Router.current().url ).indexOf( path ) > -1 && path isnt '/' ) then 'active' else ''

# @@ Determines if it's the first pick of round
# {{ isNewRound }}
Handlebars.registerHelper 'isNewRound', ->
  @pick == 1

# @@ Determines if it's an even-numbered round
# {{ isEvenRound }}
Handlebars.registerHelper 'isEvenRound', ->
  @round % 2 == 0

# @@ Determines if one value matches another
# {{ isMatch }}
Handlebars.registerHelper 'isSelected', (a, b) ->
  if a == b
    return 'selected'

# @@ Determines if position is not defense
# {{ notDefense }}
Handlebars.registerHelper 'notDefense', ->
  @position != 'dst'

# @@ Convert integer into word
# {{ getNumberWord }}
Handlebars.registerHelper 'getNumberWord', (_num) ->
  
  ints = [2, 4, 6, 8, 10, 12, 14, 16]
  words = ['two', 'four', 'six', 'eight', 'ten', 'twelve', 'fourteen', 'sixteen']

  if typeof _num == 'number'
    _index = ints.indexOf(_num)
    _num = if _index != -1 then words[_index] else _num

  return _num

# @@ Build layout for draftboardAdmin
# {{ draftboardAdmin }}
Handlebars.registerHelper 'draftboardAdmin', (summary) ->
  
  html = ''
  i = 0
  matrix = _PickMatrix.find({}, transform: (item) ->
    item.teams = _Teams.find({})
    item
  ).fetch()

  for i of matrix
    `i = i`

    teams = matrix[i]['teams'].fetch()

    if matrix[i]['pick'] == 1
      html += '<div class="row">'

    html += '<div class="column">'
    html += '<div class="cell">'
    html += '<div>' + matrix[i]['round'] + '.' + matrix[i]['pick'] + '</div>'

    html += '<select class="overall">'
    
    for x of teams
      `x = x`
      is_selected = if teams[x]['order'] == matrix[i]['team']['order'] then 'selected' else ''
      html += '<option data-team-id="' + teams[x]._id + '" data-order="' + teams[x].order + '" data-overall="' + matrix[i].overall + '" ' + is_selected + '>' + teams[x].name + '</option>'

    html += '</select>'
    html += '</div>'
    html += '</div>'

    if matrix[i]['pick'] == summary.total_teams
      html += '</div>'

    i++

  return html

# @@ Build layout for draftBoard
# {{ draftBoard }}
Handlebars.registerHelper 'draftBoard', (summary) ->
  
  html = ''
  i = 0
  matrix = _PickMatrix.find({}).fetch()

  for i of matrix
    `i = i`

    player = null
    cell_bg = ''
    if matrix[i]['overall'] < summary.overall
      pick = _Picks.findOne({ overall: matrix[i]['overall'] })
      player = _Players.findOne({ _id: pick['player_id'] })
      cell_bg = player.position.toLowerCase()

    is_otc = if matrix[i]['overall'] == summary.overall then true else false
    _otc = if is_otc then 'on-the-clock' else ''

    if matrix[i]['pick'] == 1
      html += '<div class="row">'

    html += '<div class="column">'
    html += '<div class="pick cell ' + _otc + ' ' + cell_bg + '">'
    html += '<div class="overall">' + matrix[i].round + '.' + matrix[i].pick + '</div>'
    if is_otc
      html += '<span class="timer"></span>'
    if typeof matrix[i]['traded_from'] != 'undefined'
      html += '<div class="traded_to"><i class="share icon"></i> ' + matrix[i].team.name + '</div>'
    if player != null
      html += '<div class="team-position">' + player.position + ' - ' + player.team + '</div>'
      html += '<div class="name">'
      html += '<div class="first_name">' + player.first_name + '</div>'
      html += '<div class="last_name">' + player.last_name + '</div>'
      html += '</div>'

    html += '</div>'
    html += '</div>'

    if matrix[i]['pick'] == summary.total_teams
      html += '</div>'

    i++

  return html