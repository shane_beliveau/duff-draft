Router.configure
  layoutTemplate  : '_global'
  loadingTemplate : '_loading'

# -----------------------------------------------------------------------------

Router.map ->

  @route 'draft-room', 
    path        : '/draft-room'
    template    : 'DraftRoom'
    controller  : DraftRoom

  @route 'teams', 
    path        : '/teams/:id?'
    template    : 'Teams'
    controller  : Teams

  @route 'overview', 
    path        : '/overview/:round?'
    template    : 'Overview'
    controller  : Overview

  @route 'reset', 
    path        : '/reset-all/:league?'
    template    : '_loading'

  @route 'big-board', 
    path        : '/big-board'
    template    : 'bigboard'
    controller  : BigBoard

  @route 'draft-board', 
    path            : '/'
    layoutTemplate  : ''
    template        : 'DraftBoard'
    controller      : DraftBoard

  @route 'trade-picks', 
    path        : '/trade-picks'
    template    : 'DraftPicks'
    controller  : DraftPicks