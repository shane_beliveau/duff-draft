$ ->

  # Transaction settings
  ############################################################################
  
  # Remove permissions
  tx.requireUser = false
  tx.last_transaction = false
  tx.adminLock = 0

  setInterval () ->
    tx.adminLock = 0
  , 3000

  # Undo / Redo controls
  $(document).on 'click', '.tx-action', (e) ->

    action = $(this).data('action')

    # Janky way of protecting the undo/redo actions
    if action is 'authorize'
      
      tx.adminLock += 1 

      if tx.adminLock %% 5 == 0
        
        $(this)
          .find('nav a.tx-action')
          .removeClass('hide--all')

    if action is 'undo'
      tx.undo()

    if action is 'redo'
      tx.redo()

  # Set up observer on players collection
  ############################################################################

  _Teams.find().observeChanges
    changed: (id, fields) ->

      team = _Teams.findOne(id)
      draft = _Draftboard.findOne {}
      next_pick = _Teams.findOne( 'order': draft.team_pick )
      alert = team.name + ' selects ' + team.players[team.players.length-1].first_name + ' ' + team.players[team.players.length-1].last_name + '.'
      alert_next = next_pick.name + ' is on the clock!'

      # Show the on-screen notification
      if team.players.length > 0
        sAlert.info(alert)

      # Update the sorting if applied
      _index = +window.tablesort.index
      _direction = window.tablesort.direction

      if _index != null and _direction != null
        window.tablesort.sort($('table th').eq(_index), _direction)

      window.getOnTheClockTimer true