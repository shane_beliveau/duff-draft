class @BigBoard extends BaseController
  
  fastRender: true
    
  data: ->

    _.extend super(),

      qb: ->
        _Players.find {
          drafted: null
          position: 'qb'
        }, 
        sort: adp: 1
      
      rb: ->
        _Players.find {
          drafted: null
          position: 'rb'
        }, 
        sort: adp: 1
      
      wr: ->
        _Players.find {
          drafted: null
          position: 'wr'
        }, 
        sort: adp: 1
      
      te: ->
        _Players.find {
          drafted: null
          position: 'te'
        }, 
        sort: adp: 1
      
      k: ->
        _Players.find {
          drafted: null
          position: 'k'
        }, 
        sort: adp: 1

      def: ->
        _Players.find {
          drafted: null
          position: 'dst'
        }, 
        sort: adp: 1

      isBigBoard: true
