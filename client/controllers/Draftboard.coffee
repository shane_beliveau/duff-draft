class @DraftBoard extends BaseController
  
  fastRender: true
    
  data: ->

    _.extend super(),

      teams: ->

        _Teams.find {},
          sort: 
            order: 1

      players: ->
        _Players.find {
          drafted: null
        },
        sort: 
          ranking: 1
        limit: 200

      is_draft_over: ->
        draft = _Draftboard.findOne()
        if draft.round > draft.total_rounds then true else false