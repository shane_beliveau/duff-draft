class @DraftRoom extends BaseController
  
  fastRender: true
    
  data: ->

    _.extend super(),    

      players: ->
        _Players.find {
          drafted: null
        },
        sort: 
          adp: 1
        limit: 150

      is_draft_over: ->
        draft = _Draftboard.findOne()
        if draft.round > draft.total_rounds then true else false