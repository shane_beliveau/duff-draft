class @BaseController extends RouteController

  waitOn: ->
    [ 
      Meteor.subscribe 'players'
      Meteor.subscribe 'teams'
      Meteor.subscribe 'draftboard'
      Meteor.subscribe 'picks'
      Meteor.subscribe 'pickmatrix'
    ]

  data: ->

    summary: ->

      _Draftboard.findOne {}, 
        transform: (item) ->
          item.team = _Teams.findOne(order: item.team_pick)
          item

    on_the_clock: ->
      
      _Draftboard.findOne {}, 
        transform: (item) ->
          item.otc = _PickMatrix.findOne(overall: item.overall)
          item

    on_deck: ->

      _Draftboard.findOne {}, 
        transform: (item) ->
          item.matrix = _PickMatrix.find { overall: $gt: item.overall }, limit: 12
          item

  action: ->
    if !@ready()
      @render '_loading'
    else
      @render()
    return

  fastRender: true