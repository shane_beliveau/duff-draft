class @Teams extends BaseController
  
  fastRender: true
    
  data: ->

    filter = if this.params.id then { _id : this.params.id } else {}

    _.extend super(),
    
      teams: ->
        
        _Teams.find {},
          sort: 
            order: 1

      team: ->

        _Teams.findOne filter,
          sort:
            order: 1

          transform: (item) ->
            
            players  = item.players

            qbs = $.grep(players, (e) ->
              e.position.toLowerCase() == 'qb'
            )

            rbs = $.grep(players, (e) ->
              e.position.toLowerCase() == 'rb'
            )

            wrs = $.grep(players, (e) ->
              e.position.toLowerCase() == 'wr'
            )

            tes = $.grep(players, (e) ->
              e.position.toLowerCase() == 'te'
            )

            ks = $.grep(players, (e) ->
              e.position.toLowerCase() == 'k'
            )

            defs = $.grep(players, (e) ->
              e.position.toLowerCase() == 'dst'
            )

            sort_lineup = (lineup) ->
              pre = []
              post = []
              pre.push $.grep(lineup, (e) ->
                e.position.toLowerCase() == 'qb'
              )
              pre.push $.grep(lineup, (e) ->
                e.position.toLowerCase() == 'rb'
              )
              pre.push $.grep(lineup, (e) ->
                e.position.toLowerCase() == 'wr'
              )
              pre.push $.grep(lineup, (e) ->
                e.position.toLowerCase() == 'te'
              )
              pre.push $.grep(lineup, (e) ->
                e.position.toLowerCase() == 'k'
              )
              pre.push $.grep(lineup, (e) ->
                e.position.toLowerCase() == 'dst'
              )
              post.concat pre[0], pre[1], pre[2], pre[3], pre[4], pre[5]

            for i of players
              `i = i`
              if typeof qbs[0] != 'undefined' and qbs[0]._id == players[i]._id or typeof rbs[0] != 'undefined' and rbs[0]._id == players[i]._id or typeof rbs[1] != 'undefined' and rbs[1]._id == players[i]._id or typeof wrs[0] != 'undefined' and wrs[0]._id == players[i]._id or typeof wrs[1] != 'undefined' and wrs[1]._id == players[i]._id or typeof wrs[2] != 'undefined' and wrs[2]._id == players[i]._id or typeof tes[0] != 'undefined' and tes[0]._id == players[i]._id or typeof ks[0] != 'undefined' and ks[0]._id == players[i]._id or typeof defs[0] != 'undefined' and defs[0]._id == players[i]._id
                players[i].starter = true
              else
                players[i].reserve = true
            
            starters = $.grep(players, (e) ->
              e.starter == true
            )

            reserves = $.grep(players, (e) ->
              e.reserve == true
            )

            item.starters = sort_lineup(starters)
            item.reserves = sort_lineup(reserves)
            
            item