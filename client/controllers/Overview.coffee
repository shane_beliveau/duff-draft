class @Overview extends BaseController

  fastRender: true
    
  data: ->
    
    teams = _Teams.find().fetch()
    players = _Players.find().fetch()

    _.extend super(),
      
      picks: ->
        _Picks.find {},
          
          sort: 'overall': 1
          
          transform: (item) ->

            item.team = ->
              for i of teams
                if teams[i]._id == item.team_id
                  return teams[i]
              return

            item.player = ->
              for i of players
                if players[i]._id == item.player_id
                  return players[i]
              return

            item

      draftNotStarted: ->
        draft = _Draftboard.findOne()
        if typeof draft == 'undefined' then true else draft.overall == 1
        