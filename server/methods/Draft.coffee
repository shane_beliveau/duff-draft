Meteor.methods
  
  reset_all: (league) ->
    
    league = if typeof league != 'undefined' then league else 'duff'

    _Players.remove {}
    _Draftboard.remove {}
    _Teams.remove {}
    _Picks.remove {}
    _PickMatrix.remove {}
    
    if( tx? ) 
      tx.Transactions.remove {}

    Meteor.call 'load', league

    return

  find_by_order: ->

    # Get current draft pick
    draft = _Draftboard.findOne {}

    # Find the team that made the pick
    team  = _Teams.findOne(order: draft.team_pick)
    
    # Return the team object
    team

  get_next_pick: (_overall) ->

    # Get team for current pick
    matrix = _PickMatrix.findOne(overall: _overall)

    return matrix.team
