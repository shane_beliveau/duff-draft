Meteor.methods
  
  # Load all of the players.
  load: (league) ->
    
    # Determine which league to load.
    league = if typeof league != 'undefined' then league else 'duff'

    # Load draft settings
    if not _Draftboard.find().count()
      
      # Grab JSON file
      _draftboard = JSON.parse Assets.getText 'json/' + league + '-draft.json'

      # Load draftboard
      _Draftboard.insert _draftboard

    # If the collection is empty, load all of the players from JSON
    if not _Players.find().count()

      # Grab JSON file
      _players = JSON.parse Assets.getText 'json/ffpro_overall.json'

      # Load players
      _.each _players, (player) ->
        _Players.insert player
        return

    # Load all teams if they haven't been already
    if not _Teams.find().count()

      # Grab JSON file
      _teams = JSON.parse Assets.getText 'json/' + league + '-teams.json'

      # Load players
      _.each _teams, (team) ->
        
        _team = _Teams.insert team

        # Process keepers
        if !_.isEmpty(team.keepers) and _.isArray(team.keepers)

          _.each team.keepers, (keeper) ->

            player = _Players.findOne {
              'first_name': keeper.first_name
              'last_name': keeper.last_name
            }
            
            if player

              # Set the selected player status as drafted
              _Players.update player._id,
                $set: 
                  drafted: 1
              
              # Add the player to the team
              _Teams.update _team, 
                $addToSet: 
                  players: player

        return

    # Load the pick matrix based on draft settings
    if not _PickMatrix.find().count()

      # Get the total number of picks
      _draft_settings = _Draftboard.findOne {}
      _total_picks = _draft_settings.total_rounds * _draft_settings.total_teams

      # Assign a team to every pick
      x = 1

      while x <= _total_picks

        _round     = Math.floor((x - 1) / _draft_settings.total_teams + 1)
        _pick      = (x - 1) % _draft_settings.total_teams + 1
        _team_pick = if _round % 2 then _pick else _round * _draft_settings.total_teams - x + 1

        _team  = _Teams.findOne(order: _team_pick)

        _PickMatrix.insert
          team   : _team
          round  : _round
          pick   : _pick
          overall: x

        x++

      if league == 'duff'

        # Grab JSON file
        _matrix = JSON.parse Assets.getText 'json/' + league + '-custom-draft-order.json'

        # Load players
        _.each _matrix, (pick) ->

          # Fields to update
          data = team: pick.team

          # Check to see if current pick was a traded pick
          # and define who it was from if that's the case
          current_pick = _PickMatrix.findOne overall: pick.overall

          if current_pick.team.name != pick.team.name
            data.traded_from = current_pick.team

          # Update draft order and make note of trades
          _PickMatrix.update { overall: pick.overall }, 
            $set: 
              data

          return

      return
