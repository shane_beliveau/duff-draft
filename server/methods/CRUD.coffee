Meteor.methods
  
  select: (Collection, parameters) ->
    global[Collection].find parameters

  selectOne: (Collection, parameters) ->
    global[Collection].findOne parameters

  insert: (Collection, parameters) ->
    global[Collection].insert parameters

  update: (Collection, id, parameters) ->
    global[Collection].update id, parameters

  delete: (Collection, parameters) ->
    global[Collection].remove parameters