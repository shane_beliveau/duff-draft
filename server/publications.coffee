# Permissions
# ----------------------------------------------------------------------------

_Players.allow
  insert: -> true
  update: -> true
  remove: -> true

_Teams.allow
  insert: -> true
  update: -> true
  remove: -> true

_Draftboard.allow
  insert: -> true
  update: -> true
  remove: -> true

_Picks.allow
  insert: -> true
  update: -> true
  remove: -> true

_PickMatrix.allow
  insert: -> true
  update: -> true
  remove: -> true


# Publications
# ----------------------------------------------------------------------------

# Publish the players
Meteor.publish 'players', ->
  _Players.find()

# Publish draftboard
Meteor.publish 'draftboard', ->
  _Draftboard.find()

# Publish teams
Meteor.publish 'teams', ->
  _Teams.find()

# Publish picks
Meteor.publish 'picks', ->
  _Picks.find()

# Publish pick matrix
Meteor.publish 'pickmatrix', ->
  _PickMatrix.find()