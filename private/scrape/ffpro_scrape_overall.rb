require 'mechanize'
require 'pp'
require 'json'

# Check if we need to process images
process_images = ARGV[0]

# Empty JSON file
File.open(File.dirname(__FILE__) + '/../json/ffpro_overall.json', 'w')

source = 'https://www.fantasypros.com/nfl/adp/ppr-overall.php'

# Set globals
@hashes = []

mechanize = Mechanize.new

page = mechanize.get( source )

rows = page.search('table#data tr')

rows.each do |row|

  first_name = (row.search('td.player-label a.player-name').text).partition(' ').first
  last_name = (row.search('td.player-label a.player-name').text).partition(' ').last
  team = row.search('td.player-label small:nth-of-type(1)').text
  bye = (row.search('td.player-label small:nth-of-type(2)').text).sub(/([^0-9]+)/, '')
  position_rank = row.search('td[3]').text
  position = (row.search('td[3]').text).sub(/([^A-Za-z]+)/, '')
  ranking = row.search('td[1]').text
  adp = row.search('td[8]').text

  # Get the safe name
  safe_first_name = first_name.gsub(/[^0-9a-z]/i,'').downcase
  safe_last_name = last_name.gsub(/[^0-9a-z]/i,'').downcase

  # Adjust the ADP number
  adp = adp.delete(',').to_i

  if( adp != 0 )

    player = {
      :ranking        => ranking.to_i,
      :adp            => adp,
      :first_name     => first_name,
      :last_name      => last_name,
      :position       => position.downcase,
      :position_rank  => position_rank,
      :team           => team,
      :bye            => bye.to_i
    }

    if( process_images == 'process_images' )
      player[:image] = File.file?(File.dirname(__FILE__) + "/../../public/images/players/#{safe_last_name}_#{safe_first_name}.png") ? "#{safe_last_name}_#{safe_first_name}.png" : 'no_image.png'
    end

    @hashes << player

  end

end

# Write to file
File.open(File.dirname(__FILE__) + '/../json/ffpro_overall.json', 'a') do |file|
  file.write( JSON.pretty_generate(@hashes) )
end