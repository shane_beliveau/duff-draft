require 'mechanize'
require 'pp'
require 'json'

@hashes = []
@images = []

mechanize = Mechanize.new

page = mechanize.get('https://www.espn.com/fantasy/football/story/_/id/26415022/fantasy-football-updated-2019-ppr-rankings-mike-clay')

rows = page.search('.article-body aside.inline-table:nth-of-type(2) table tr.last')

rows.each do |row|
    
  if match = row.at('td[1]').text.match(/(\d*)\.\s(.*?)\s(.*)/i)

    overall, first_name, last_name = match.captures

    safe_first_name = first_name.gsub(/[^0-9a-z]/i,'').downcase
    safe_last_name = last_name.gsub(/[^0-9a-z]/i,'').downcase

    if link = row.at('td[1] a')
        
      link = link['href'].scan(/([^\/]+)(?=\/[^\/]+\/?$)/i).first.join('').to_i

      if link != 0 && !File.exist?("./public/images/players/#{safe_last_name}_#{safe_first_name}.png")
        begin
          img = mechanize.get("https://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/#{link}.png&w=250")
          puts "Downloading ... #{safe_last_name}_#{safe_first_name}.png"
          img.save "./public/images/players/#{safe_last_name}_#{safe_first_name}.png"
        rescue StandardError
          puts "There is no such page."
        end
      end

    end

  end

end